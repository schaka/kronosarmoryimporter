#include <string>

// defines here
std::string delimiter = "\"";

inline int GetIntValueFromLineWithIndenticator(std::string line, std::string identicator)
{
    if (size_t pos = line.find(identicator))
    {
        int posLength = pos + identicator.length();
        if (size_t pos2 = line.find(delimiter, posLength))
            return atoi(line.substr(pos + identicator.length(), pos2 - posLength).c_str());
    }

    return 0;
}

inline std::string GetStringFromLineWithIndenticator(std::string line, std::string identicator)
{
    if (size_t pos = line.find(identicator))
    {
        int posLength = pos + identicator.length();
        if (size_t pos2 = line.find(delimiter, posLength))
            return line.substr(pos + identicator.length(), pos2 - posLength);
    }

    return "";
}