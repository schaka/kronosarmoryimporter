#include "precompiled.h"

enum ArmoryAdressType
{
    ARMORY_ADRESS_TYPE_CHARACTER = 0,
    ARMORY_ADRESS_TYPE_REPUTATION,
    ARMORY_ADRESS_TYPE_MAX
};

struct ReputationData
{
    int FactionEntry;
    std::string FactionName;
    int FactionStanding;
};

struct CharacterData
{
    std::string CharacterName;
    int CharacterLevel;
    int RaceEntry;
    int ClassEntry;
    int FactionEntry;
    int KnownTitles;
    int ActiveTitle;
    std::string ActiveTitleString;
};

struct PvPData
{
    int WeeklyKills;
    int LifetimeKills;
    int HighestRank;
    int Rank;
};

struct SkillData
{
    int SkillEntry;
    int SkillValue;
    int SkillMax;
    std::string SkillName;
};

struct ItemData // [slot]
{
    int ItemEntry;
    int EnchantEntry;
};

enum EquipmentSlots
{
    EQUIPMENT_SLOT_START = 0,
    EQUIPMENT_SLOT_HEAD = 0,
    EQUIPMENT_SLOT_NECK = 1,
    EQUIPMENT_SLOT_SHOULDERS = 2,
    EQUIPMENT_SLOT_BODY = 3,
    EQUIPMENT_SLOT_CHEST = 4,
    EQUIPMENT_SLOT_WAIST = 5,
    EQUIPMENT_SLOT_LEGS = 6,
    EQUIPMENT_SLOT_FEET = 7,
    EQUIPMENT_SLOT_WRISTS = 8,
    EQUIPMENT_SLOT_HANDS = 9,
    EQUIPMENT_SLOT_FINGER1 = 10,
    EQUIPMENT_SLOT_FINGER2 = 11,
    EQUIPMENT_SLOT_TRINKET1 = 12,
    EQUIPMENT_SLOT_TRINKET2 = 13,
    EQUIPMENT_SLOT_BACK = 14,
    EQUIPMENT_SLOT_MAINHAND = 15,
    EQUIPMENT_SLOT_OFFHAND = 16,
    EQUIPMENT_SLOT_RANGED = 17,
    EQUIPMENT_SLOT_TABARD = 18,
    EQUIPMENT_SLOT_END = 19
};

class ArmoryImporter
{
public:
    ArmoryImporter(std::string characterName, std::string realmName);

    const std::string DownloadArmoryData();
    bool ParseData(std::string response);

    typedef std::vector<ReputationData> ReputationDataList;
    typedef std::vector<SkillData> SkillDataList;

    const ReputationDataList & GetReputationData() { return _reputationDataList; }
    const SkillDataList & GetSkillData() { return _skillDataList; }

    const CharacterData GetCharacterData() { return _characterData; }
    const PvPData GetPvPData() { return _pvpData; }
    const ItemData GetItemDataForSlot(int slot) { return _itemData[slot]; }

private:
    bool ValidateData(std::string response);

    const std::string BuildArmoryAdressString(ArmoryAdressType armoryAdressType);

    std::string _realmName;
    std::string _characterName;
    std::string _armoryHost;
    std::string _armoryAdress[ARMORY_ADRESS_TYPE_MAX];

    SkillDataList _skillDataList;
    ReputationDataList _reputationDataList;

    CharacterData _characterData;
    PvPData _pvpData;
    ItemData _itemData[EQUIPMENT_SLOT_END];
};
