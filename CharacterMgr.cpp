#include "CharacterMgr.h"

#define MAX_TITLES 15

TitleData Titles[] =
{
    { 1, 15, "Private", "Scout" },
    { 2, 16, "Corporal", "Grunt" },
    { 3, 17, "Sergeant", "Sergeant" },
    { 4, 18, "Master Sergeant", "Senior Sergeant" },
    { 5, 19, "Sergeant Major", "First Sergeant" },
    { 6, 20, "Knight", "Stone Guard" },
    { 7, 21, "Knight-Lieutenant", "Blood Guard" },

    { 8, 22, "Knight-Captain", "Legionnaire" },
    { 9, 23, "Knight-Champion", "Centurion" },
    { 10, 24, "Lieutenant Commander", "Champion" },
    { 11, 25, "Commander", "Lieutenant General" },
    { 12, 26, "Marshal", "General" },
    { 13, 27, "Field Marshal", "Warlord" },
    { 14, 28, "Grand Marshal", "High Warlord" },
};

CharacterMgr::CharacterMgr() {}

CharacterMgr::~CharacterMgr() {}

const FactionEntry CharacterMgr::GetFactionEntryForRace(int race)
{
    switch (race)
    {
        case RACE_HUMAN:
        case RACE_DWARF:
        case RACE_NIGHTELF:
        case RACE_GNOME:
        case RACE_DRAENEI:
            return FACTION_ENTRY_ALLIANCE;
        case RACE_ORC:
        case RACE_UNDEAD_PLAYER:
        case RACE_TAUREN:
        case RACE_TROLL:
        case RACE_BLOODELF:
            return FACTION_ENTRY_HORDE;
        default:
            return FACTION_ENTRY_OTHER;
    }
}

const int CharacterMgr::GetTitleEntryForFaction(int faction, int rank)
{
    int titleEntry;
    if (faction == FACTION_ENTRY_HORDE)
    {
        titleEntry = Titles[rank].HordeTitleEntry;
    }
    else
    {
        titleEntry = Titles[rank].AllianceTitleEntry;
    }

    return titleEntry;
}

const std::string CharacterMgr::GetTitleStringForFaction(int faction, int rank)
{
    std::string titleString;
    if (faction == FACTION_ENTRY_HORDE)
    {
        titleString = Titles[rank].HordeTitleName;
    }
    else
    {
        titleString = Titles[rank].AllianceTitleName;
    }

    return titleString;
}

const int CharacterMgr::GetKnownTitleMaskFromHighestRank(int faction, int rank)
{
    int titleMask = 0;
    for (int i = 0; i < rank; ++i)
    {
        int titleEntry = (faction == FACTION_ENTRY_HORDE) ? Titles[i].HordeTitleEntry : Titles[i].AllianceTitleEntry;
        titleMask += (1 << titleEntry);
    }

    return titleMask;
}

const int CharacterMgr::GetTitleEntryForFactionFromString(int faction, std::string activeTitleString)
{
    for (int i = 0; i < MAX_TITLES; ++i)
    {
        std::string titleName = (faction == FACTION_ENTRY_HORDE) ? Titles[i].HordeTitleName : Titles[i].AllianceTitleName;
        if (activeTitleString.compare(titleName) == 0)
            return (faction == FACTION_ENTRY_HORDE) ? Titles[i].HordeTitleEntry : Titles[i].AllianceTitleEntry;
    }

    return 0;
}
