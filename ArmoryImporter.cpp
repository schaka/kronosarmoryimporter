#include "ArmoryImporter.h"
#include "CharacterMgr.h"
#include "util.h"

using boost::asio::ip::tcp;

ArmoryImporter::ArmoryImporter(std::string characterName, std::string realmName)
{
    _characterName = characterName;
    _realmName = realmName;
    _armoryHost = "armory.twinstar.cz";
    _armoryAdress[ARMORY_ADRESS_TYPE_CHARACTER] = BuildArmoryAdressString(ARMORY_ADRESS_TYPE_CHARACTER);
    _armoryAdress[ARMORY_ADRESS_TYPE_REPUTATION] = BuildArmoryAdressString(ARMORY_ADRESS_TYPE_REPUTATION);
}

const std::string ArmoryImporter::BuildArmoryAdressString(ArmoryAdressType armoryAdressType)
{
    std::string str = "/character-";
    str.append(armoryAdressType == ARMORY_ADRESS_TYPE_CHARACTER  ? "sheet" : "reputation");
    str.append(".xml?r=");
    str.append(_realmName);
    str.append("&cn=");
    str.append(_characterName);
    return str;
}

const std::string ArmoryImporter::DownloadArmoryData()
{
    boost::asio::io_service io_service;
    // Get a list of endpoints corresponding to the server name.
    tcp::resolver resolver(io_service);
    tcp::resolver::query query(_armoryHost, "http");
    tcp::resolver::iterator endpoint_iterator = resolver.resolve(query);
    tcp::resolver::iterator end;
    // Try each endpoint until we successfully establish a connection.
    tcp::socket socket(io_service);
    boost::system::error_code error = boost::asio::error::host_not_found;
    while (error && endpoint_iterator != end)
    {
        socket.close();
        socket.connect(*endpoint_iterator++, error);
    }

    // Form the request. We specify the "Connection: close" header so that the
    // server will close the socket after transmitting the response. This will
    // allow us to treat all data up until the EOF as the content.
    boost::asio::streambuf request[ARMORY_ADRESS_TYPE_MAX];
    for (int i = 0; i < ARMORY_ADRESS_TYPE_MAX; ++i)
    {
        std::ostream request_stream(&request[i]);
        request_stream << "GET " << _armoryAdress[i] << " HTTP/1.0\r\n";
        request_stream << "Host: " << _armoryHost << "\r\n";
        request_stream << "Accept: */*\r\n";
        request_stream << "Connection: close\r\n\r\n";

        // Send the request.
        boost::asio::write(socket, request[i]);
    }

    // Read the response status line.
    boost::asio::streambuf response;
    boost::asio::read_until(socket, response, "\r\n");

    // Check that response is OK.
    std::istream response_stream(&response);
    std::string http_version;
    response_stream >> http_version;
    unsigned int status_code;
    response_stream >> status_code;
    std::string status_message;
    std::getline(response_stream, status_message);

    // Read the response headers, which are terminated by a blank line.
    boost::asio::read_until(socket, response, "\r\n\r\n");

    // Process the response headers.
    std::string header;
    while (std::getline(response_stream, header) && header != "\r") {}

    // Read until EOF, writing data to output as we go.
    while (boost::asio::read(socket, response, boost::asio::transfer_at_least(1), error)) {}
    
    boost::asio::streambuf::const_buffers_type buffer = response.data();
    std::string str(boost::asio::buffers_begin(buffer), boost::asio::buffers_begin(buffer) + response.size());
    return str;
}

bool ArmoryImporter::ValidateData(std::string response)
{
    std::istringstream iss(response);
    std::string line;
    while (std::getline(iss, line))
    {
        // try to get error
        if (line.find("_layout/error/error.xsl") != std::string::npos)
        {
            // LOG
            return false;
        }
    }

    return true;
}

bool ArmoryImporter::ParseData(std::string response)
{
    if (!ValidateData(response))
    {
        std::cout << "Error!";
        return false;
    }

    // parse reputation data first
    std::istringstream iss(response);
    std::string line;

    while (getline(iss, line))
    {
        // Faction Info
        if (line.find("faction id") != std::string::npos)
        {
            ReputationData reputationData;

            reputationData.FactionEntry = GetIntValueFromLineWithIndenticator(line, "<faction id=\"");
            reputationData.FactionStanding = GetIntValueFromLineWithIndenticator(line, " reputation=\"");
            reputationData.FactionName = GetStringFromLineWithIndenticator(line, " name=\"");

            _reputationDataList.push_back(reputationData);
        }

        // Character Info
        if (line.find("character ") != std::string::npos)
        {
            _characterData.CharacterName = GetStringFromLineWithIndenticator(line, " name=\"");
            _characterData.CharacterLevel = GetIntValueFromLineWithIndenticator(line, " level=\"");
            _characterData.ClassEntry = GetIntValueFromLineWithIndenticator(line, " classId=\"");
            _characterData.RaceEntry = GetIntValueFromLineWithIndenticator(line, " raceId=\"");

            // Title Data
            std::string indenticator = " prefix=\"";
            _characterData.ActiveTitleString = GetStringFromLineWithIndenticator(line, indenticator);
            if (_characterData.ActiveTitleString.empty())
            {
                indenticator = " suffix=\"";
                _characterData.ActiveTitleString = GetStringFromLineWithIndenticator(line, indenticator);
                if (_characterData.ActiveTitleString.empty())
                    std::cout << "No selected title" << std::endl;
            }
        }

        // PvP Info
        if (line.find("honorRanking ") != std::string::npos)
        {
            _pvpData.WeeklyKills = GetIntValueFromLineWithIndenticator(line, "killsWeek=\"");
            _pvpData.LifetimeKills = GetIntValueFromLineWithIndenticator(line, " kills=\"");
            _pvpData.HighestRank = GetIntValueFromLineWithIndenticator(line, " rankHighest=\"");
            _pvpData.Rank = GetIntValueFromLineWithIndenticator(line, " rank=\"");
        }

        // Skill Info
        if (line.find("skill id") != std::string::npos)
        {
            SkillData skillData;

            skillData.SkillEntry = GetIntValueFromLineWithIndenticator(line, "<skill id=\"");
            skillData.SkillValue = GetIntValueFromLineWithIndenticator(line, " value=\"");
            skillData.SkillMax = GetIntValueFromLineWithIndenticator(line, " max=\"");
            skillData.SkillName = GetStringFromLineWithIndenticator(line, " name=\"");
            _skillDataList.push_back(skillData);
        }

        // Item Info
        if (line.find("item displayInfoId") != std::string::npos)
        {
            int slot = GetIntValueFromLineWithIndenticator(line, " slot=\"");
            _itemData[slot].ItemEntry = GetIntValueFromLineWithIndenticator(line, " id=\"");
            _itemData[slot].EnchantEntry = GetIntValueFromLineWithIndenticator(line, " permanentenchant=\"");
        }
    }

    // Character Info
    _characterData.FactionEntry = sCharacterMgr->GetFactionEntryForRace(_characterData.RaceEntry);

    // Title Info
    _characterData.KnownTitles = sCharacterMgr->GetKnownTitleMaskFromHighestRank(_characterData.FactionEntry, _pvpData.HighestRank);
    _characterData.ActiveTitle = sCharacterMgr->GetTitleEntryForFactionFromString(_characterData.FactionEntry, _characterData.ActiveTitleString);

    return true;
}
