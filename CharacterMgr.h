#include "precompiled.h"

enum Races
{
    RACE_NONE = 0,
    RACE_HUMAN = 1,
    RACE_ORC = 2,
    RACE_DWARF = 3,
    RACE_NIGHTELF = 4,
    RACE_UNDEAD_PLAYER = 5,
    RACE_TAUREN = 6,
    RACE_GNOME = 7,
    RACE_TROLL = 8,
    //RACE_GOBLIN             = 9,
    RACE_BLOODELF = 10,
    RACE_DRAENEI = 11
    //RACE_FEL_ORC            = 12,
    //RACE_NAGA               = 13,
    //RACE_BROKEN             = 14,
    //RACE_SKELETON           = 15,
    //RACE_VRYKUL             = 16,
    //RACE_TUSKARR            = 17,
    //RACE_FOREST_TROLL       = 18,
    //RACE_TAUNKA             = 19,
    //RACE_NORTHREND_SKELETON = 20,
    //RACE_ICE_TROLL          = 21
};

enum FactionEntry
{
    FACTION_ENTRY_HORDE = 67,
    FACTION_ENTRY_ALLIANCE = 469,
    FACTION_ENTRY_OTHER = 0
};

enum TitleRanks
{
    TITLE_RANK_0 = 0,
    TITLE_RANK_1,
    TITLE_RANK_2,
    TITLE_RANK_3,
    TITLE_RANK_4,
    TITLE_RANK_5,
    TITLE_RANK_6,
    TITLE_RANK_7,
    TITLE_RANK_8,
    TITLE_RANK_9,
    TITLE_RANK_10,
    TITLE_RANK_11,
    TITLE_RANK_12,
    TITLE_RANK_13
};

struct TitleData // [rank]
{
    int AllianceTitleEntry;
    int HordeTitleEntry;
    std::string AllianceTitleName;
    std::string HordeTitleName;
};

class CharacterMgr
{
private:
    CharacterMgr();
    ~CharacterMgr();

public:
    static CharacterMgr* instance()
    {
        static CharacterMgr instance;
        return &instance;
    }

    const FactionEntry GetFactionEntryForRace(int race);
    const int GetTitleEntryForFaction(int faction, int rank);
    const std::string GetTitleStringForFaction(int faction, int rank);
    const int GetKnownTitleMaskFromHighestRank(int faction, int rank);
    const int GetTitleEntryForFactionFromString(int faction, std::string activeTitleString);
};

#define sCharacterMgr CharacterMgr::instance()